# Implementasi MVC + Go Lang + Frontend
Pertemuan BackEnd - 25. Implementasi MVC + Go Lang + Frontend
- MVC - for structure
- React and Redux - for FrontEnd
- NodeJS (React and Redux) - for FrontEnd
- GoLang - for BackEnd 
> GoLang `postman` running on (http://localhost:8081/)

> NodeJS - npm `browser` running on (http://localhost:3000/)

this repo tried both environments

---

## `--- environment setup ---`

<details>
<summary>"Klik disini - tutorial dummies for the first time. in case I forgot"</summary>

### 1. `NodeJS` session
<details>
<summary>"Klik disini untuk melihat session"</summary>

- `check` nodejs and npm
```
node -version
npm -version
```
- `file` init
jangan lupa inisialisasi - untuk bikin `package.json` **kalau belum ada** dengan
```
npm init
```

</details>

### 2. `React` session
<details>
<summary>"Klik disini untuk melihat detail session"</summary>

- `membuat` folder baru **otomatis**, di VScode terminal
```
npx create-react-app namafolderbaru
```
- `running` react dengan
```
npm start
```
kelar langsung kebuka default [port](http://localhost:3000/)

>> **only after you have no error**, then do: 
```
npm run build
```
browser taking a long time, indeed.

</details>

### 3. `GoLang` session
<details>
<summary>"Klik disini untuk melihat golang session"</summary>

- `running` golang di terminal VScode
```
go run namafile.go
```
- `inisialisasi` modul
untuk buat file `go.mod` dan `go.sum` di direktori kerja. 
```
go mod init namadirektori
``` 
- ambil `package` setelah init
```
go get -u namaurl.com
```

</details>

</details>

Happy coding!

---

## `--- from template step by step ---`
<details>
<summary>"Klik disini untuk melihat detail steps"</summary>

### 1. `clone`
- `template` from [simplebank-redux](https://github.com/kartikaaekap/simplebank-redux)

### 2. `error` npm setelah instalasi npm
<details>
<summary>"Klik untuk detail errornya di npm"</summary>

in the process, if you move template to your own repo, git will make a .gitignore for node_modules folder.

this will ignore thousands of files created by ```npm install``` which will takes a long time to install.

After installation, npm may return [red error](#), use
```
npm audit fix
``` 
to fix your npm.

</details>

### 3. `running` npm
<details>
<summary>"Klik me again"</summary>

running the template by starting npm
```
npm start
```

</details>

### 4. `localhost` npm
- running on [port](https://localhost:3000)

</details>

---

# `--- DOKUMENTASI foto dan REST API ---`
<details>
<summary>"Klik disini untuk melihat dokumentasi"</summary>

- halaman login awal install
![halamanlogin](/uploads/8e887a7ca594343807021e7ca7bfb5d5/halamanlogin.JPG)

- halaman login register
![halamanloginregister](/uploads/1a210ac73517780741c47abba587a65b/halamanloginregister.JPG)

- dashboard berhasil login warning
![halamanloginregister](/uploads/7cb790efb4c525752486584d17609726/halamanloginregister.JPG)

- cek login hash key
![cek_browser_login_cookie_key](/uploads/d484bedc8c556efc230c507384bcba77/cek_browser_login_cookie_key.JPG)

</details>

---

## ` --- for further learning ---`
knowledge never ends
<details>
<summary>"Klik saya"</summary>

### > `react`
React  tersusun dari komponen-komponen

**Ada 2 jalan**
1. Class
2. Function Component

### > `markdown`
```
<details>
<summary>"Klik saya"</summary>
You're doing an awesome work!
</details>
```

</details>

---